//#include "Includes.hpp"
//
//#include "ShaderGL.hpp"
//
//
//
//int main() {
//
//    // Initialize OpenGL and everything related
//    glfwInit();
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
//    glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
//    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
//
//    GLFWwindow* window = glfwCreateWindow(100, 100, "Window", nullptr, nullptr);
//    glfwMakeContextCurrent(window);
//
//    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
//	
//
//
//    // Create shader to add two arrays together
//    ShaderGL shaderAddArrays;
//    shaderAddArrays.addShaderStage("shaderAddArrays.comp", ShaderStage::COMPUTE);
//    shaderAddArrays.createProgram();
//
//    // Create vectors to add together
//    std::vector<int> a = { 1,2,3,4,5 };
//    std::vector<int> b = { 10,20,30,40,50 };
//    std::vector<int> c(a.size());
//
//    // Create OpenGL buffers and set the data, the third buffer only allocates memory, doesnt set anything
//    std::array<uint32_t, 3> buffers;
//    glCreateBuffers(3, buffers.data());
//    glNamedBufferStorage(buffers[0], a.size() * sizeof(int), a.data(), GL_DYNAMIC_STORAGE_BIT);
//    glNamedBufferStorage(buffers[1], b.size() * sizeof(int), b.data(), GL_DYNAMIC_STORAGE_BIT);
//    glNamedBufferStorage(buffers[2], c.size() * sizeof(int), nullptr, GL_DYNAMIC_STORAGE_BIT);
//
//    // Bind the buffers to specific binding points, they must match inside the shader
//    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers[0]);
//    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, buffers[1]);
//    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffers[2]);
//
//    // Use shader and dispatch groups of threads 
//    // Inside the shader you specify how large each group will be
//    // Here you specify how many groups you want, also in the x, y and z axes
//    // Since each group is 256 in the x axis we need N / 256 groups along the x axis
//    // If the number is not evenly divisible by 256 we add one more group 
//    // and handle the rest that we dont need in the shader
//    shaderAddArrays.use();
//    shaderAddArrays.setInt("arraySize", a.size());
//    glDispatchCompute(a.size() / 256 + (a.size() % 256 > 0), 1, 1);
//    glMemoryBarrier(GL_ALL_BARRIER_BITS);
//
//    // Retrieve data that was computed on the GPU
//    glGetNamedBufferSubData(buffers[2], 0, c.size() * sizeof(int), c.data());
//
//    // Print result on screen
//    for (int i = 0; i < c.size(); i++) {
//        printf("%d\n", c[i]);
//    }
//
//    glDeleteBuffers(3, buffers.data());
//
//	return 0;
//}