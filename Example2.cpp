#include "Includes.hpp"

#include "ShaderGL.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"



int main() {

    // Initialize OpenGL and everything related
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(100, 100, "Window", nullptr, nullptr);
    glfwMakeContextCurrent(window);

    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

    

    // Create shader to invert image
    ShaderGL shaderInvertImage;
    shaderInvertImage.addShaderStage("shaderInvertImage.comp", ShaderStage::COMPUTE);
    shaderInvertImage.createProgram();

    // Load image into memory
    glm::ivec2 size;
    int components;
    glm::u8vec4* image = (glm::u8vec4*)stbi_load("example.jpeg", &size.x, &size.y, &components, 4);

    // Create OpenGL texture
    uint32_t texture;
    glCreateTextures(GL_TEXTURE_2D, 1, &texture);
    glTextureStorage2D(texture, 1, GL_RGBA8, size.x, size.y);
    glTextureSubImage2D(texture, 0, 0, 0, size.x, size.y, GL_RGBA, GL_UNSIGNED_BYTE, image);
    glTextureParameteri(texture, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // Bind the texture to a binding point and specify access type and data type
    glBindImageTexture(0, texture, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA8);
    
    // Use shader and dispatch groups of threads to cover the whole image (both x and y axes)
    shaderInvertImage.use();
    shaderInvertImage.setVector2i("size", size);
    glDispatchCompute(size.x / 16 + (size.x % 16 > 0), size.y / 16 + (size.y % 16 > 0), 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);

    // Retrieve data that was computed on the GPU
    glGetTextureImage(texture, 0, GL_RGBA, GL_UNSIGNED_BYTE, size.x * size.y * sizeof(glm::u8vec4), image);

    stbi_write_png("exampleInverted.png", size.x, size.y, 4, image, 0);

    stbi_image_free(image);
    glDeleteTextures(1, &texture);

    return 0;
}