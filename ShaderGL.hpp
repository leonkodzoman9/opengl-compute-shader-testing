#pragma once

#include "Includes.hpp"



using ShaderID = uint32_t;
using ShaderStageID = uint32_t;
using UniformLocation = uint32_t;



enum class ShaderStage : uint32_t {
    VERTEX = GL_VERTEX_SHADER,
    FRAGMENT = GL_FRAGMENT_SHADER,
    COMPUTE = GL_COMPUTE_SHADER
};



class ShaderGL {

private:

    ShaderID shaderID = 0;

public:

    ShaderGL() = default;
    ~ShaderGL();

    ShaderGL(const ShaderGL& rhs) = delete;
    ShaderGL& operator=(const ShaderGL& rhs) = delete;
    ShaderGL(ShaderGL&& rhs) = delete;
    ShaderGL& operator=(ShaderGL&& shader) noexcept;

    void addShaderStage(std::string shaderPath, ShaderStage stage);
    void createProgram();

    void use() const;



    void setInt(std::string_view locationName, int value) const;
    void setVector2i(std::string_view locationName, glm::ivec2 vector) const;
    void setVector3i(std::string_view locationName, glm::ivec3 vector) const;
    void setVector4i(std::string_view locationName, glm::ivec4 vector) const;

    void setIntArray(std::string_view locationName, int* values, int count) const;
    void setVector2iArray(std::string_view locationName, glm::ivec2* vector, int count) const;
    void setVector3iArray(std::string_view locationName, glm::ivec3* vector, int count) const;
    void setVector4iArray(std::string_view locationName, glm::ivec4* vector, int count) const;

    void setFloat(std::string_view locationName, float value) const;
    void setVector2f(std::string_view locationName, glm::vec2 vector) const;
    void setVector3f(std::string_view locationName, glm::vec3 vector) const;
    void setVector4f(std::string_view locationName, glm::vec4 vector) const;

    void setFloatArray(std::string_view locationName, float* value, int count) const;
    void setVector2fArray(std::string_view locationName, glm::vec2* vector, int count) const;
    void setVector3fArray(std::string_view locationName, glm::vec3* vector, int count) const;
    void setVector4fArray(std::string_view locationName, glm::vec4* vector, int count) const;

    void setMatrix2f(std::string_view locationName, glm::mat2 matrix, bool transpose = false) const;
    void setMatrix3f(std::string_view locationName, glm::mat3 matrix, bool transpose = false) const;
    void setMatrix4f(std::string_view locationName, glm::mat4 matrix, bool transpose = false) const;

    void setMatrix2fArray(std::string_view locationName, glm::mat2* matrices, int count, bool transpose = false) const;
    void setMatrix3fArray(std::string_view locationName, glm::mat3* matrices, int count, bool transpose = false) const;
    void setMatrix4fArray(std::string_view locationName, glm::mat4* matrices, int count, bool transpose = false) const;

private:

    void checkShaderForErrors(ShaderStageID ID) const;
    void checkProgramForErrors() const;
};






