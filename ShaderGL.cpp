#include "ShaderGL.hpp"



uint32_t getShaderType(ShaderStage type) {

    switch (type) {
    case ShaderStage::VERTEX:      return GL_VERTEX_SHADER;
    case ShaderStage::FRAGMENT:    return GL_FRAGMENT_SHADER;
    case ShaderStage::COMPUTE:     return GL_COMPUTE_SHADER;
    }

    return 0;
}



ShaderGL::~ShaderGL() {

    glDeleteProgram(this->shaderID);
}

ShaderGL& ShaderGL::operator=(ShaderGL&& shader) noexcept {

    if (this != &shader) {
        glDeleteProgram(this->shaderID);
        this->shaderID = std::exchange(shader.shaderID, 0);
    }

    return *this;
}

void ShaderGL::addShaderStage(std::string shaderPath, ShaderStage stage) {

    std::ifstream source(shaderPath);

    std::stringstream shaderSource;
    if (source.is_open()) {
        shaderSource << source.rdbuf();
        source.close();
    }
    else {
        printf("Unable to open file <%s>.\n", shaderPath.data());
        source.close();
        throw;
    }

    const ShaderStageID stageID = glCreateShader(getShaderType(stage));

    std::string shaderSourceString = shaderSource.str();
    const char* shaderPtr = shaderSourceString.data();
    glShaderSource(stageID, 1, &shaderPtr, nullptr);
    glCompileShader(stageID);

    this->checkShaderForErrors(stageID);

    if (this->shaderID == 0) {
        this->shaderID = glCreateProgram();
    }
    glAttachShader(this->shaderID, stageID);
}

void ShaderGL::createProgram() {

    glLinkProgram(this->shaderID);

    this->checkProgramForErrors();
}

void ShaderGL::use() const {

    glUseProgram(this->shaderID);
}



void ShaderGL::setInt(std::string_view locationName, int value) const {

    glUniform1i(glGetUniformLocation(this->shaderID, locationName.data()), value);
}
void ShaderGL::setVector2i(std::string_view locationName, glm::ivec2 vector) const {

    glUniform2i(glGetUniformLocation(this->shaderID, locationName.data()), vector.x, vector.y);
}
void ShaderGL::setVector3i(std::string_view locationName, glm::ivec3 vector) const {

    glUniform3i(glGetUniformLocation(this->shaderID, locationName.data()), vector.x, vector.y, vector.z);
}
void ShaderGL::setVector4i(std::string_view locationName, glm::ivec4 vector) const {

    glUniform4i(glGetUniformLocation(this->shaderID, locationName.data()), vector.x, vector.y, vector.z, vector.w);
}

void ShaderGL::setIntArray(std::string_view locationName, int* values, int count) const {

    glUniform1iv(glGetUniformLocation(this->shaderID, locationName.data()), count, values);
}
void ShaderGL::setVector2iArray(std::string_view locationName, glm::ivec2* values, int count) const {

    glUniform2iv(glGetUniformLocation(this->shaderID, locationName.data()), count, (int*)values);
}
void ShaderGL::setVector3iArray(std::string_view locationName, glm::ivec3* values, int count) const {

    glUniform3iv(glGetUniformLocation(this->shaderID, locationName.data()), count, (int*)values);
}
void ShaderGL::setVector4iArray(std::string_view locationName, glm::ivec4* values, int count) const {

    glUniform4iv(glGetUniformLocation(this->shaderID, locationName.data()), count, (int*)values);
}

void ShaderGL::setFloat(std::string_view locationName, float value) const {

    glUniform1f(glGetUniformLocation(this->shaderID, locationName.data()), value);
}
void ShaderGL::setVector2f(std::string_view locationName, glm::vec2 vector) const {

    glUniform2f(glGetUniformLocation(this->shaderID, locationName.data()), vector.x, vector.y);
}
void ShaderGL::setVector3f(std::string_view locationName, glm::vec3 vector) const {

    glUniform3f(glGetUniformLocation(this->shaderID, locationName.data()), vector.x, vector.y, vector.z);
}
void ShaderGL::setVector4f(std::string_view locationName, glm::vec4 vector) const {

    glUniform4f(glGetUniformLocation(this->shaderID, locationName.data()), vector.x, vector.y, vector.z, vector.w);
}

void ShaderGL::setFloatArray(std::string_view locationName, float* values, int count) const {

    glUniform1fv(glGetUniformLocation(this->shaderID, locationName.data()), count, values);
}
void ShaderGL::setVector2fArray(std::string_view locationName, glm::vec2* values, int count) const {

    glUniform2fv(glGetUniformLocation(this->shaderID, locationName.data()), count, (float*)values);
}
void ShaderGL::setVector3fArray(std::string_view locationName, glm::vec3* values, int count) const {

    glUniform3fv(glGetUniformLocation(this->shaderID, locationName.data()), count, (float*)values);
}
void ShaderGL::setVector4fArray(std::string_view locationName, glm::vec4* values, int count) const {

    glUniform4fv(glGetUniformLocation(this->shaderID, locationName.data()), count, (float*)values);
}

void ShaderGL::setMatrix2f(std::string_view locationName, glm::mat2 matrix, bool transpose) const {

    glUniformMatrix2fv(glGetUniformLocation(this->shaderID, locationName.data()), 1, transpose, &matrix[0][0]);
}
void ShaderGL::setMatrix3f(std::string_view locationName, glm::mat3 matrix, bool transpose) const {

    glUniformMatrix3fv(glGetUniformLocation(this->shaderID, locationName.data()), 1, transpose, &matrix[0][0]);
}
void ShaderGL::setMatrix4f(std::string_view locationName, glm::mat4 matrix, bool transpose) const {

    glUniformMatrix4fv(glGetUniformLocation(this->shaderID, locationName.data()), 1, transpose, &matrix[0][0]);
}

void ShaderGL::setMatrix2fArray(std::string_view locationName, glm::mat2* matrices, int count, bool transpose) const {

    glUniformMatrix2fv(glGetUniformLocation(this->shaderID, locationName.data()), count, transpose, (float*)matrices);
}
void ShaderGL::setMatrix3fArray(std::string_view locationName, glm::mat3* matrices, int count, bool transpose) const {

    glUniformMatrix3fv(glGetUniformLocation(this->shaderID, locationName.data()), count, transpose, (float*)matrices);
}
void ShaderGL::setMatrix4fArray(std::string_view locationName, glm::mat4* matrices, int count, bool transpose) const {

    glUniformMatrix4fv(glGetUniformLocation(this->shaderID, locationName.data()), count, transpose, (float*)matrices);
}



void ShaderGL::checkShaderForErrors(const ShaderStageID ID) const {

    int success;
    char infoLog[1024];
    glGetShaderiv(ID, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(ID, 1024, NULL, infoLog);
        printf("Unable to compile shader. \nError log : %s\n", infoLog);
        throw;
    }
}
void ShaderGL::checkProgramForErrors() const {

    int success;
    char infoLog[1024];
    glGetProgramiv(this->shaderID, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(this->shaderID, 1024, NULL, infoLog);
        printf("Unable to link shaders. \nError log : %s\n", infoLog);
        throw;
    }
}


