#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <iostream>
#include <fstream>

#include <cmath>
#include <cstdint>

#include <chrono>
#include <algorithm>
#include <numeric>
#include <random>

#include <array>
#include <vector>
#include <string>



